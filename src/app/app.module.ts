import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CategoryComponent } from './category/category.component';
import { CartingComponent } from './carting/carting.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { FooterComponent } from './shared/components/footer/footer.component';
import { BreadcrumbComponent } from './shared/components/breadcrumb/breadcrumb.component';
import { HeaderMobComponent } from './shared/components/header-mob/header-mob.component';
import { BreadcrumbMobComponent } from './shared/components/breadcrumb-mob/breadcrumb-mob.component';
import { FooterMobComponent } from './shared/components/footer-mob/footer-mob.component';
import { ProductCarouselComponent } from './shared/components/product-carousel/product-carousel.component';
import { MainComponent } from './shared/components/main/main.component';
import { SidebarComponent } from './shared/components/sidebar/sidebar.component';
import { CategoryMenuComponent } from './shared/components/category-menu/category-menu.component';
import { CategoryMenuMobComponent } from './shared/components/category-menu-mob/category-menu-mob.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductFilterComponent } from './shared/components/product-filter/product-filter.component';
import { ProductFilterMobComponent } from './shared/components/product-filter-mob/product-filter-mob.component';
import { ProductItemComponent } from './shared/components/product-item/product-item.component';
import { PaginationComponent } from './shared/components/pagination/pagination.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CategoryComponent,
    CartingComponent,
    HeaderComponent,
    FooterComponent,
    BreadcrumbComponent,
    HeaderMobComponent,
    BreadcrumbMobComponent,
    FooterMobComponent,
    ProductCarouselComponent,
    MainComponent,
    SidebarComponent,
    CategoryMenuComponent,
    CategoryMenuMobComponent,
    ProductDetailComponent,
    ProductFilterComponent,
    ProductFilterMobComponent,
    ProductItemComponent,
    PaginationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

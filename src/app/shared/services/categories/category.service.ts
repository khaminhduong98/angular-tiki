import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Category} from './category';

const categoriesApi = 'assets/api/categories.json';

@Injectable({
  providedIn: 'root'
})

export class CategoryService {

  constructor(
    private http: HttpClient
  ) { }

  getCategories(): Observable<{categories: Array<Category>}> {
    return this.http.get<{categories: Array<Category>}>(categoriesApi);
  }
}

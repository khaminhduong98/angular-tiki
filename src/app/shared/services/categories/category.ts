export interface Category {
  id: string;
  name: string;
  quantity: string;
}

import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Filter} from './filter';

const filtersApi = 'assets/api/filters.json';

@Injectable({
  providedIn: 'root'
})
export class FilterService {

  constructor(
    private http: HttpClient
  ) { }

  getFilters(): Observable<{filters: Array<Filter>}> {
    return this.http.get<{filters: Array<Filter>}>(filtersApi);
  }
}

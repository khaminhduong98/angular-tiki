export interface Product {
  id: string;
  name: string;
  price: string;
  discPercentage: string;
  discPrice: string;
  shippingTime: string;
  sellerName: string;
  starRating: string;
  imageUrl: string;
}

// export interface MinhProduct{
//   products: Array<Product>;
// }

import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable, of} from 'rxjs';

import {Product} from './product';
import {catchError, retry} from 'rxjs/operators';

const productsApi = 'assets/api/products.json';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(
    private http: HttpClient
  ) { }

  getProducts(): Observable<{products: Array<Product>}> {
    return this.http.get<{products: Array<Product>}>(productsApi);
  }

  // getProductById(id: any): Observable<any> {
  //   return this.http.get<Product>(productsApi + id).pipe(
  //     retry(3), catchError(this.handleError<Product>('getProduct')));
  // }

  // tslint:disable-next-line:typedef
  // private handleError<T>(operation = 'operation', result?: T) {
  //   return (error: any): Observable<T> => {
  //     console.error(error);
  //     this.log(`${operation} failed: ${error.message}`);
  //
  //     return of(result as T);
  //   };
  // }
  //
  // tslint:disable-next-line:typedef
  // private log(message: string) {
  //   console.log(message);
  // }
}

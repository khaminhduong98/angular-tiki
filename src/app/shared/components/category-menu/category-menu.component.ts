import { Component, OnInit } from '@angular/core';
import {CategoryService} from "../../services/categories/category.service";
import {Product} from "../../services/products/product";
import {Category} from "../../services/categories/category";

@Component({
  selector: 'app-category-menu',
  templateUrl: './category-menu.component.html',
  styleUrls: ['./category-menu.component.scss']
})
export class CategoryMenuComponent implements OnInit {

  categories: Category[] = [];

  constructor(
    private categoryService: CategoryService
  ) { }

  ngOnInit(): void {
    this.getCategories();
  }

  getCategories() {
    this.categoryService.getCategories()
      .subscribe(resp => {
        // const keys = resp.headers.keys();
        // this.headers = keys.map(key =>
        //   `${key}: ${resp.headers.get(key)}`);

        for (const data of resp.categories) {
          this.categories.push(data);
        }
        console.log(this.categories);
      });
  }
}

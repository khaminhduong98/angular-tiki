import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryMenuMobComponent } from './category-menu-mob.component';

describe('CategoryMenuMobComponent', () => {
  let component: CategoryMenuMobComponent;
  let fixture: ComponentFixture<CategoryMenuMobComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategoryMenuMobComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryMenuMobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {Category} from "../../services/categories/category";
import {CategoryService} from "../../services/categories/category.service";

@Component({
  selector: 'app-category-menu-mob',
  templateUrl: './category-menu-mob.component.html',
  styleUrls: ['./category-menu-mob.component.scss']
})
export class CategoryMenuMobComponent implements OnInit {

  categories: Category[] = [];

  constructor(
    private categoryService: CategoryService
  ) { }

  ngOnInit(): void {
    this.getCategories();
  }

  getCategories() {
    this.categoryService.getCategories()
      .subscribe(resp => {
        // const keys = resp.headers.keys();
        // this.headers = keys.map(key =>
        //   `${key}: ${resp.headers.get(key)}`);

        for (const data of resp.categories) {
          this.categories.push(data);
        }
        console.log(this.categories);
      });
  }
}

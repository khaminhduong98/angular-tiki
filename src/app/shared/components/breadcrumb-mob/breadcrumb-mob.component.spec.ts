import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BreadcrumbMobComponent } from './breadcrumb-mob.component';

describe('BreadcrumbMobComponent', () => {
  let component: BreadcrumbMobComponent;
  let fixture: ComponentFixture<BreadcrumbMobComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BreadcrumbMobComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BreadcrumbMobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

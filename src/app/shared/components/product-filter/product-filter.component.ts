import { Component, OnInit } from '@angular/core';
import {Category} from "../../services/categories/category";
import {Filter} from "../../services/filters/filter";
import {CategoryService} from "../../services/categories/category.service";
import {FilterService} from "../../services/filters/filter.service";

@Component({
  selector: 'app-product-filter',
  templateUrl: './product-filter.component.html',
  styleUrls: ['./product-filter.component.scss']
})
export class ProductFilterComponent implements OnInit {

  filters: Filter[] = [];

  constructor(
    private filterService: FilterService
  ) { }

  ngOnInit(): void {
    this.getFilters();
  }

  getFilters() {
    this.filterService.getFilters()
      .subscribe(resp => {
        // const keys = resp.headers.keys();
        // this.headers = keys.map(key =>
        //   `${key}: ${resp.headers.get(key)}`);

        for (const data of resp.filters) {
          this.filters.push(data);
        }
        console.log(this.filters);
      });
  }
}

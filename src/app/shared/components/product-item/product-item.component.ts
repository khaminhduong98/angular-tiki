import { Component, OnInit } from '@angular/core';
import {Product} from '../../services/products/product';
import {ProductService} from '../../services/products/product.service';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.scss']
})
export class ProductItemComponent implements OnInit {

  products: Product[] = [];

  constructor(
    private productService: ProductService
  ) {
  }

  ngOnInit(): void {
    this.getProducts();
    // this.getProductById();
  }

  getProducts() {
    this.productService.getProducts()
      .subscribe(resp => {
        // const keys = resp.headers.keys();
        // this.headers = keys.map(key =>
        //   `${key}: ${resp.headers.get(key)}`);

        for (const data of resp.products) {
          this.products.push(data);
        }
        console.log(this.products);
      });
  }

  // getProductById(id: any) {
  //   this.productService.getProductById(id)
  //     .subscribe(data => {
  //       console.log(data);
  //     });
  // }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductFilterMobComponent } from './product-filter-mob.component';

describe('ProductFilterMobComponent', () => {
  let component: ProductFilterMobComponent;
  let fixture: ComponentFixture<ProductFilterMobComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductFilterMobComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductFilterMobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

// Sidebar menu on Mobile
function collapseSidebar() {
  let x = document.getElementById("sidebarMob");
  if (x.style.display === "block") {
    x.style.display = "none";
  } else {
    x.style.display = "block";
  }
}
